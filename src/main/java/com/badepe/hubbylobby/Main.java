package com.badepe.hubbylobby;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by New-Wave on 2014.03.07..
 */
public class Main extends JavaPlugin{
    public Location spawnloc;
    public Inventory petinv;
	public String prefix = "[HubbyLobby]"
    public void onEnable() {
        getLogger().info("The HubbyLobby Plugin Started");
        getServer().getPluginManager().registerEvents(new Listener(), this);
        petinv.addItem();
    }
    public void onDisable() {
        getLogger().info("The HubbyLobby Plugin Disabled");
    }
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(label.equalsIgnoreCase("spawnpoint")){
                spawnloc = ((Player) sender).getLocation();
        }
        return false;
    }
}
